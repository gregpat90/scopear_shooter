﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class GameModelTest
{
	[Test]
	public void DefaultScore() {
        var gameModel = new GameModel();
        Assert.AreEqual(0, gameModel.Score);
	}

    [Test]
    public void UpdatingScore()
    {
        var gameModel = new GameModel();
        bool changeEventCalled = false;

        uint newScore = 150;

        gameModel.OnModelChange += (sender, arg) =>
        {
            changeEventCalled = true;
            Assert.AreSame(gameModel, sender);
            Assert.AreEqual("Score", arg.PropertyName);
            Assert.AreEqual(newScore, gameModel.Score);
        };

        gameModel.Score = newScore;

        Assert.IsTrue(changeEventCalled);
    }

}
