﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{

    [SerializeField]
    private Text ScoreText;

	// Use this for initialization
	void Start () {
        GameObject gameControllerObject = GameController.GetGameCotrollerGameObject();
        if (gameControllerObject == null)
        {
            Debug.LogError("Could not find game controller object");
            return;
        }

        GameController gameController = gameControllerObject.GetComponent<GameController>();
        if (gameController == null)
        {
            Debug.LogError("Could not find GameController component of the GameController game object");
            return;
        }

        gameController.Model.OnModelChange += OnModelChange;

        ScoreText.text = gameController.Model.Score.ToString();
    }

    private void OnModelChange(object sender, ModelChangedEventArgs e)
    {
        ScoreText.text = (sender as IGameModel).Score.ToString();
    }
}
