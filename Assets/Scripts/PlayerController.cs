﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Player Controller is responsible the player GameObject
/// includes logic for looking and movement and firing projectiles
/// Require a Rigid Body for movement.
/// Requires a Camera component in a child game object
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    //Name of the axis used for input
    private static readonly string HORIZONTAL_LOOK_AXIS_NAME = "Horizontal";
    private static readonly string VERTICAL_LOOK_AXIS_NAME = "Vertical";
    private static readonly string FORWARD_MOVEMENT_AXIS_NAME = "Forward";
    private static readonly string STRAFE_MOVEMENT_AXIS_NAME = "Strafe";
    private static readonly string FIRE_AXIS_NAME = "Fire";

    //Store a reference to the rigid body   
    private Rigidbody mRigidbody;

    //Store a reference to th players camera
    //This camera should be some where in a child;
    private Camera mPlayerCamera;

    [SerializeField]
    private float VerticalLookInputChangeCoefficent = 10.0f;
    [SerializeField]
    private float HorizontalLookInputChangeCoefficent = 50.0f;

    [SerializeField]
    [Tooltip("Movement speed in meters per second")]
    private float MovementSpeed = 0.2f;

    [SerializeField]
    [Tooltip("The delay before the next projectile can be fired")]
    private float NextPorjectileDelay = 1.0f;

    //should be assigned for the editor;
    [SerializeField]
    private GameObject ProjectilePrefab;

    private float mCurrentCameraAngle = 0;
    private float mTimeDeltaFromLastProjectile = 0;

	// Use this for initialization
	void Start ()
    {
        mRigidbody = GetComponent<Rigidbody>();

        //preset the time delta so that we can fire right away
        mTimeDeltaFromLastProjectile = NextPorjectileDelay;

        //try to get the players camera from an child game object;
        mPlayerCamera = GetComponentInChildren<Camera>();
        if(mPlayerCamera == null)
        {
            Debug.LogError("Could not get player Camera");
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        //process input 
        ProcessLookInput();

        ProcessBodyMovementInput();

        ProcessFireProjectileInput();
    }

    /// <summary>
    /// Processes input and adjust the direction the player is looking
    /// </summary>
    private void ProcessLookInput()
    {
        //rotate the player via the rigid body 
        float horizontalInput = Input.GetAxis(HORIZONTAL_LOOK_AXIS_NAME);
        var newRotation = this.transform.rotation * Quaternion.AngleAxis(horizontalInput * HorizontalLookInputChangeCoefficent * Time.deltaTime, Vector3.up);
        mRigidbody.MoveRotation(newRotation);

        //rotate the camera around it's z axis  
        float verticalInput = Input.GetAxis(VERTICAL_LOOK_AXIS_NAME);
        if (mPlayerCamera != null)
        {
            mCurrentCameraAngle += verticalInput * VerticalLookInputChangeCoefficent * Time.deltaTime;
            mCurrentCameraAngle = Mathf.Clamp(mCurrentCameraAngle, -45f, 45f);
            mPlayerCamera.transform.localRotation = Quaternion.AngleAxis(mCurrentCameraAngle, Vector3.left);
        }
    }

    /// <summary>
    /// Processes input and moves the players "body" 
    /// </summary>
    private void ProcessBodyMovementInput()
    {
        // move the rigid body forward/backward and strafe left or Right relative the forward direction of the player  
        float forwardInput = Input.GetAxis(FORWARD_MOVEMENT_AXIS_NAME);
        float strafeInput = Input.GetAxis(STRAFE_MOVEMENT_AXIS_NAME);

        Vector3 movementDirection = (this.transform.forward * forwardInput + this.transform.right * -strafeInput);
        movementDirection.Normalize();

        //change the position base on the direction and the players movement speed
        Vector3 changeInPosition = movementDirection * (MovementSpeed * Time.deltaTime);

        mRigidbody.MovePosition(this.transform.position + changeInPosition);
    }

    /// <summary>
    /// Processes input and fire a new projectile if conditions are meet
    /// in the direction the player is looking
    /// </summary>
    private void ProcessFireProjectileInput()
    {
        //increase the time since projectile by the delta from the last update
        mTimeDeltaFromLastProjectile += Time.deltaTime;

        bool canFire = mTimeDeltaFromLastProjectile >= NextPorjectileDelay;

        if (Input.GetAxis(FIRE_AXIS_NAME) > 0.0f && canFire)
        {
            //compute the direction the projectile should be fired
            Vector3 projectileDirection = mPlayerCamera.transform.TransformDirection(Vector3.forward);

            // offset the initial position 
            Vector3 initialPostion = mPlayerCamera.transform.position + (projectileDirection * 2);

            GameObject newProjectileObject = Instantiate(ProjectilePrefab, initialPostion, Quaternion.identity);

            Projectile projectile = newProjectileObject.GetComponent<Projectile>();

            projectile.Fire(projectileDirection);

            //rest the time from last projectile to 0;
            mTimeDeltaFromLastProjectile = 0;
        }

    }
}
