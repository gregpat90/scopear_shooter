﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Projectile is used to "Hit" an enemy
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class Projectile : MonoBehaviour
{

    // Hold a reference to attached rigid body
    private Rigidbody mRigidBody;

    [SerializeField]
    [Tooltip("The initial speed of the projectile when fired")]
    private float InitalSpeed = 25.0f;

    // Use this for initialization
    void Awake () {
        mRigidBody = GetComponent<Rigidbody>();
	}

    /// <summary>
    ///  Fires the projectile in the given direction in world space 
    ///  the projectile will an initial speed of "InitalSpeed"
    /// </summary>
    /// <param name="direction"></param>
    public void Fire(Vector3 direction)
    {
        direction.Normalize();
        mRigidBody.AddForce(direction * InitalSpeed, ForceMode.VelocityChange);
    }

    /// <summary>
    /// Unity built in event for when this object is part of a collision
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }
}
