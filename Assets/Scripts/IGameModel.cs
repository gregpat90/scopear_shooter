﻿using System;

/// <summary>
/// Event Args for when a property of the model has changed
/// </summary>
public class ModelChangedEventArgs : EventArgs
{
    public string PropertyName { get; set; }
}

/// <summary>
/// Model for high level Game information 
/// </summary>
public interface IGameModel
{
    /// <summary>
    /// The Score of the Game
    /// </summary>
    uint Score
    {
        get;
    }

    /// <summary>
    /// Event Handler for event when a property in the model changes
    /// </summary>
    event EventHandler<ModelChangedEventArgs> OnModelChange;
}