﻿
using System;
/// <summary>
/// Implementation of the Game Model
/// </summary>
public class GameModel : IGameModel
{
    private uint mScore;
    public uint Score
    {
        get
        {
            return mScore;
        }

        set
        {
            if(value != mScore)
            {
                //the score has changed update the private variable and invoke the callback
                mScore = value;
                if(OnModelChange != null)
                {
                    OnModelChange.Invoke(this, new ModelChangedEventArgs { PropertyName = "Score" });
                }
            }
        }
    }

    public event EventHandler<ModelChangedEventArgs> OnModelChange;
}