﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Game Controller is used to manipulate the high level game play objectives
/// </summary>
public class GameController : MonoBehaviour
{

    //Name of the tag that should be used acquire the game object that has this component 
    private static readonly string GAME_CONTROLLER_TAG = "GameController";

    [SerializeField]
    private uint PointsPerEnemyDestroyed = 100;

    /// <summary>
    /// retrieves the game object marked with the "GameController" tag
    /// </summary>
    /// <returns>A GameObject tagged with "GameController" or null</returns>
    public static GameObject GetGameCotrollerGameObject()
    {
        return GameObject.FindGameObjectWithTag(GAME_CONTROLLER_TAG);
    }

    private GameModel mModel = new GameModel();
    /// <summary>
    /// Game model exposed via the IGameInterface
    /// </summary>
    public IGameModel Model
    {
        get { return mModel; }
    }
    
    /// <summary>
    /// Informs the game the an enemy was destroyed
    /// </summary>
    public void EnemyDestoryed()
    {
        mModel.Score = mModel.Score + PointsPerEnemyDestroyed;
    }
}
