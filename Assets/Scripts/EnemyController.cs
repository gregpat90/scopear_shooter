﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enemy Controller used to control behavior of enemy entities 
/// Responds to being hit with projectiles 
/// </summary>
public class EnemyController : MonoBehaviour
{
    
    //the name of the tag for projectile game objects that we want to take a "hit" from 
    private static readonly string PROJECTILE_TAG = "Projectile";

    /// <summary>
    /// Built in unity event for when a collision happens
    /// This is used to detect when a projectile collides with the enemy 
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        // we should on responds to hits from game objects tagged as projectiles
        // this should always be false id the physics layers a set up properly
        if (collision.gameObject.tag != PROJECTILE_TAG)
        {
            Debug.LogWarning("A collision was detected with a non projectile named: " + collision.gameObject.name);
            return;
        }

        // Tell the game controller that this enemy was destroyed
        GameObject gameControllerObject = GameController.GetGameCotrollerGameObject();
        if(gameControllerObject == null)
        {
            Debug.LogError("Could not find game controller object");
            return;
        }

        GameController gameController = gameControllerObject.GetComponent<GameController>();
        if (gameController == null)
        {
            Debug.LogError("Could not find GameController component of the GameController game object");
            return;
        }

        gameController.EnemyDestoryed();

        //remove the enemy from the scene;
        Destroy(this.gameObject);
    }
}
